//
//  InfoGloboUITests.swift
//  InfoGloboUITests
//
//  Created by André Lessa Guedes.
//

import XCTest

class InfoGloboUITests: XCTestCase {

    func testInterfaceHome() throws {
        let app = XCUIApplication()
        app.launch()
        
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Sérgio Cabral é denunciado pela sexta vez na Lava-Jato"]/*[[".cells.staticTexts[\"Sérgio Cabral é denunciado pela sexta vez na Lava-Jato\"]",".staticTexts[\"Sérgio Cabral é denunciado pela sexta vez na Lava-Jato\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        let backButton = app.navigationBars["BRASIL"].buttons["Back"]
        XCTAssertTrue(backButton.exists)
        backButton.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Dirceu é condenado a mais 11 anos de prisão na Lava-Jato"]/*[[".cells.staticTexts[\"Dirceu é condenado a mais 11 anos de prisão na Lava-Jato\"]",".staticTexts[\"Dirceu é condenado a mais 11 anos de prisão na Lava-Jato\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        backButton.tap()
        tablesQuery.cells.containing(.staticText, identifier:"CCJ do Senado aprova união estável e casamento entre pessoas do mesmo sexo").element.swipeUp()
        
        XCTAssertTrue(tablesQuery.cells.containing(.staticText, identifier:"CCJ do Senado aprova união estável e casamento entre pessoas do mesmo sexo").element.exists)
    }
}
