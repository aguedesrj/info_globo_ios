//
//  InfoGloboTests.swift
//  InfoGloboTests
//
//  Created by André Lessa Guedes.
//

import XCTest
@testable import InfoGlobo

class InfoGloboTests: XCTestCase {

    func testServico() throws {
        let didReceiveResponse = expectation(description: #function)
        
        let service: HomeService = HomeService()
        service.obterListaConteudo(success: { (result) in
            didReceiveResponse.fulfill()
            XCTAssertTrue(true, "Serviço retornado com sucesso!!!")
        }) { (error) in
            didReceiveResponse.fulfill()
            XCTAssertTrue(false, "ERRO AO CHAMAR SERVIÇO DE NOTÍCIAS: \(error)")
        }
        
        wait(for: [didReceiveResponse], timeout: 5)
    }
}
