//
//  HomeService.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import Foundation
import SwiftyJSON
import Alamofire

final class HomeService {
    
    /// Método responsável por realizar a chamada a url que retorna o conteúdo das notícias
    /// - Parameters:
    ///   - success: retorna a lista do conteúdo das notícias
    ///   - failure: retorna o erro na chamada a url.
    func obterListaConteudo(success: @escaping ([Conteudo]) -> Void, failure: @escaping (_ error: NSError) -> Void) {
        
        let manager: SessionManager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30 // em segundos
        manager.session.configuration.timeoutIntervalForResource = 30 // em segundos
        
        manager.request("https://raw.githubusercontent.com/Infoglobo/desafio-apps/master/capa.json",
                        method: .get,
                        parameters: nil, encoding: URLEncoding.default,
                        headers: nil)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    success(Conteudo.parse(json: [JSON(response.value!)]))
                case .failure( _):
                    failure(NSError(domain: NSURLErrorDomain,
                                    code: 500,
                                    userInfo: [NSLocalizedDescriptionKey: "Erro ao chamar o serviço do InfoGlobo."]))
                }
        }
    }
}
