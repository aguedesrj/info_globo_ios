//
//  DetalheViewController.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import UIKit

class DetalheViewController: UIViewController {
    
    var conteudo: Conteudo!
    private var tableView: UITableView!
    
    override func loadView() {
        let view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = .white
        self.view = view
        
        setupAutoLayoutTableView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(DetalheCell.self, forCellReuseIdentifier: "DetalheCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = conteudo.secao.nome.uppercased()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "share"),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: nil)
        
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController!.navigationBar.topItem!.backBarButtonItem = backButton
    }
}

extension DetalheViewController {
    
    private func setupAutoLayoutTableView() {
        tableView = UITableView(frame: view.frame)
        tableView.dataSource = self
        tableView.separatorStyle = .none
        self.view.addSubview(tableView)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}

extension DetalheViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetalheCell", for: indexPath) as! DetalheCell
        
        cell.setValuesViews(conteudo: self.conteudo)
        return cell
    }
}
