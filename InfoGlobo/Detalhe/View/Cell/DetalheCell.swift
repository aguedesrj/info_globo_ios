//
//  DetalheCell.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import UIKit
import Kingfisher

class DetalheCell: UITableViewCell {
    
    var labelTitulo: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 15.0/255.0, green: 53.0/255.0, blue: 75.0/255.0, alpha: 1.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = NSTextAlignment.center
        label.numberOfLines = 0
        label.clipsToBounds = true
        return label
    }()
    
    var labelSubTitulo: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 32.0/255.0, green: 65.0/255.0, blue: 85.0/255.0, alpha: 1.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = NSTextAlignment.center
        label.numberOfLines = 0
        label.clipsToBounds = true
        return label
    }()
    
    var labelAutor: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 154.0/255.0, green: 154.0/255.0, blue: 154.0/255.0, alpha: 1.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = NSTextAlignment.center
        label.clipsToBounds = true
        return label
    }()
    
    var labelDataHoraPublicacao: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 154.0/255.0, green: 154.0/255.0, blue: 154.0/255.0, alpha: 1.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = NSTextAlignment.center
        label.numberOfLines = 0
        label.clipsToBounds = true
        return label
    }()
    
    var imageViewFoto: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    var labelLegendaFonte: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.clipsToBounds = true
        return label
    }()
    
    var labelTexto: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.clipsToBounds = true
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        labelTitulo.font = Util.customSizeFonteBold(sizeFont: 22)
        labelSubTitulo.font = Util.customSizeFonte(sizeFont: 18)
        labelDataHoraPublicacao.font = Util.customSizeFonteBold(sizeFont: 12)
        labelLegendaFonte.font = Util.customSizeFonte(sizeFont: 12)
        labelTexto.font = Util.customSizeFonte(sizeFont: 16)
        
        self.contentView.addSubview(labelTitulo)
        self.contentView.addSubview(labelSubTitulo)
        self.contentView.addSubview(labelAutor)
        self.contentView.addSubview(labelDataHoraPublicacao)
        self.contentView.addSubview(imageViewFoto)
        self.contentView.addSubview(labelLegendaFonte)
        self.contentView.addSubview(labelTexto)
        
        labelTitulo.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        labelTitulo.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -20).isActive = true
        labelTitulo.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 20).isActive = true
        
        labelSubTitulo.topAnchor.constraint(equalTo: labelTitulo.bottomAnchor, constant: 10).isActive = true
        labelSubTitulo.rightAnchor.constraint(equalTo: labelTitulo.rightAnchor).isActive = true
        labelSubTitulo.leftAnchor.constraint(equalTo: labelTitulo.leftAnchor).isActive = true
        
        labelAutor.topAnchor.constraint(equalTo: labelSubTitulo.bottomAnchor, constant: 20).isActive = true
        labelAutor.rightAnchor.constraint(equalTo: labelTitulo.rightAnchor).isActive = true
        labelAutor.leftAnchor.constraint(equalTo: labelTitulo.leftAnchor).isActive = true
        
        labelDataHoraPublicacao.topAnchor.constraint(equalTo: labelAutor.bottomAnchor, constant: 3).isActive = true
        labelDataHoraPublicacao.rightAnchor.constraint(equalTo: labelTitulo.rightAnchor).isActive = true
        labelDataHoraPublicacao.leftAnchor.constraint(equalTo: labelTitulo.leftAnchor).isActive = true
        
        imageViewFoto.topAnchor.constraint(equalTo: labelDataHoraPublicacao.bottomAnchor, constant: 20).isActive = true
        imageViewFoto.leftAnchor.constraint(equalTo: self.contentView.leftAnchor).isActive = true
        imageViewFoto.rightAnchor.constraint(equalTo: self.contentView.rightAnchor).isActive = true
        imageViewFoto.heightAnchor.constraint(equalToConstant: (UIScreen.main.bounds.height * 25) / 100).isActive = true
        imageViewFoto.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        
        labelLegendaFonte.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 10).isActive = true
        labelLegendaFonte.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -10).isActive = true
        labelLegendaFonte.bottomAnchor.constraint(equalTo: imageViewFoto.bottomAnchor, constant: -10).isActive = true
        
        labelTexto.topAnchor.constraint(equalTo: imageViewFoto.bottomAnchor, constant: 30).isActive = true
        labelTexto.rightAnchor.constraint(equalTo: labelTitulo.rightAnchor).isActive = true
        labelTexto.leftAnchor.constraint(equalTo: labelTitulo.leftAnchor).isActive = true
        labelTexto.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -20).isActive = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

extension DetalheCell {
    
    /// Método responsável por exibir os dados do conteúdo da notícia na tela.
    /// - Parameter conteudo: conteúdo da notícia.
    func setValuesViews(conteudo: Conteudo) {
        labelTitulo.text = conteudo.titulo
        labelSubTitulo.text = conteudo.subTitulo
        
        if (conteudo.autores != "") {
            let attributedString = NSMutableAttributedString(string: "Por \(conteudo.autores)".uppercased(),
                                                             attributes: [.font: Util.customSizeFonteBold(sizeFont: 12)])
            attributedString.addAttribute(.foregroundColor,
                                          value: UIColor(red: 0.0/255.0, green: 159.0/255.0, blue: 200.0/255.0, alpha: 1.0),
                                          range: NSRange(location: 4, length: conteudo.autores.count))
            labelAutor.attributedText = attributedString
        } else {
            labelAutor.removeFromSuperview()
            labelDataHoraPublicacao.topAnchor.constraint(equalTo: labelSubTitulo.bottomAnchor, constant: 20).isActive = true
        }
        
        labelDataHoraPublicacao.text = conteudo.publicadoEm
        labelTexto.text = conteudo.texto
        
        imageViewFoto.kf.indicatorType = .activity
        if (!conteudo.imagens.isEmpty) {
            imageViewFoto.kf.setImage(with: URL(string: conteudo.imagens[0].url))
            labelLegendaFonte.text = "\(conteudo.imagens[0].legenda). Foto: \(conteudo.imagens[0].fonte)"
        } else {
            imageViewFoto.removeFromSuperview()
            labelLegendaFonte.removeFromSuperview()
            
            labelTexto.topAnchor.constraint(equalTo: labelDataHoraPublicacao.bottomAnchor, constant: 30).isActive = true
        }
    }
}
