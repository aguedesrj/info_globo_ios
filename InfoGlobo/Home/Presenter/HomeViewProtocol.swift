//
//  HomeViewProtocol.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import Foundation

protocol HomeViewProtocol {
    func success(lista: [Conteudo])
    func failure(mensagem: String)
}
