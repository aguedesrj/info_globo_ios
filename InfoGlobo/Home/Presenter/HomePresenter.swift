//
//  HomePresenter.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import Foundation

class HomePresenter {
    
    fileprivate var service: HomeService
    fileprivate var view: HomeViewProtocol
    
    init(view: HomeViewProtocol) {
        self.view = view
        self.service = HomeService()
    }
}

extension HomePresenter {
    
    /// Método responsável por obter o JSON de notícias do InfoGlobo.
    func obterConteudo() {
        service.obterListaConteudo(success: { (result) in
            self.view.success(lista: result)
        }) { (error) in
            self.view.failure(mensagem: error.description)
        }
    }
}
