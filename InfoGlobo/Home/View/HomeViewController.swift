//
//  HomeViewController.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import UIKit

class HomeViewController: UIViewController {
    
    private var presenter: HomePresenter!
    private var tableConteudo: UITableView!
    private var lista: [Conteudo] = []
    
    override func loadView() {
        let view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = .white
        self.view = view
        
        setupAutoLayoutTableView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = HomePresenter.init(view: self)
        presenter.obterConteudo()
        
        tableConteudo.register(PrincipalCell.self, forCellReuseIdentifier: "PrincipalCell")
        tableConteudo.register(NoticiasCell.self, forCellReuseIdentifier: "NoticiasCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"),
                                                                style: .plain,
                                                                target: self,
                                                                action: nil)
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo_home"))
    }
}

extension HomeViewController {
    
    private func setupAutoLayoutTableView() {
        tableConteudo = UITableView(frame: view.frame)
        tableConteudo.dataSource = self
        tableConteudo.delegate = self
        self.view.addSubview(tableConteudo)
        
        tableConteudo.translatesAutoresizingMaskIntoConstraints = false
        tableConteudo.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableConteudo.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableConteudo.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableConteudo.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}

extension HomeViewController: HomeViewProtocol {
    
    func success(lista: [Conteudo]) {
        self.lista = lista
        self.tableConteudo.reloadData()
    }
    
    func failure(mensagem: String) {
        // Exibir alerta com a mensagem de erro.
    }
}

extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PrincipalCell", for: indexPath) as! PrincipalCell
            
            cell.selectionStyle = .none
            cell.setValuesViews(conteudo: self.lista[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoticiasCell", for: indexPath) as! NoticiasCell
            
            cell.selectionStyle = .none
            cell.setValuesViews(conteudo: self.lista[indexPath.row])
            return cell
        }
    }
}

extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        HomeRouter().show(at: self.navigationController!, conteudo: self.lista[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 0) {
            return (UIScreen.main.bounds.height * 30) / 100
        }
        return (UIScreen.main.bounds.height * 11) / 100
    }
}
