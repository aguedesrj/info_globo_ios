//
//  NoticiasCell.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import UIKit
import Kingfisher

class NoticiasCell: UITableViewCell {
    
    var imageNoticia: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    var labelSecao: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 161.0/255.0,
                                  green: 185.0/255.0,
                                  blue: 193.0/255.0,
                                  alpha: 1.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var labelTitulo: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 41.0/255.0,
                                  green: 72.0/255.0,
                                  blue: 92.0/255.0,
                                  alpha: 1.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        labelSecao.font = Util.customSizeFonteBold(sizeFont: 12)
        labelTitulo.font = Util.customSizeFonte(sizeFont: 14)
        
        self.contentView.addSubview(imageNoticia)
        self.contentView.addSubview(labelSecao)
        self.contentView.addSubview(labelTitulo)
        
        imageNoticia.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        imageNoticia.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 10).isActive = true
        imageNoticia.heightAnchor.constraint(equalToConstant: (UIScreen.main.bounds.height * 8) / 100).isActive = true
        imageNoticia.widthAnchor.constraint(equalToConstant: (UIScreen.main.bounds.height * 12) / 100).isActive = true
        
        labelSecao.topAnchor.constraint(equalTo: imageNoticia.topAnchor).isActive = true
        labelSecao.leftAnchor.constraint(equalTo: imageNoticia.leftAnchor, constant: ((UIScreen.main.bounds.height * 12) / 100) + 20).isActive = true
        
        labelTitulo.topAnchor.constraint(equalTo: imageNoticia.topAnchor, constant: 20).isActive = true
        labelTitulo.leadingAnchor.constraint(equalTo: labelSecao.leadingAnchor).isActive = true
        labelTitulo.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -20).isActive = true
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

extension NoticiasCell {
    
    /// Método responsável por exibir os dados do conteúdo da notícia na tela.
    /// - Parameter conteudo: conteúdo da notícia.
    func setValuesViews(conteudo: Conteudo) {
        labelSecao.text = conteudo.secao.nome.uppercased()
        labelTitulo.text = conteudo.titulo
        
        imageNoticia.kf.indicatorType = .activity
        if (!conteudo.imagens.isEmpty) {
            imageNoticia.kf.setImage(with: URL(string: conteudo.imagens[0].url))
        } else {
            imageNoticia.kf.setImage(with: nil, placeholder: UIImage(named: "placeholder"))
        }
    }
}
