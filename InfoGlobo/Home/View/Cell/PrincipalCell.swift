//
//  PrincipalCell.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import UIKit
import Kingfisher

class PrincipalCell: UITableViewCell {
    
    var imageViewBackground: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    var labelTitulo: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.clipsToBounds = true
        return label
    }()
    
    var labelEditoria: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = UIColor.black
        label.numberOfLines = 0
        label.clipsToBounds = true
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        labelTitulo.font = Util.customSizeFonteBold(sizeFont: 18)
        labelEditoria.font = Util.customSizeFonte(sizeFont: 12)
        
        self.contentView.addSubview(imageViewBackground)
        self.contentView.addSubview(labelTitulo)
        self.contentView.addSubview(labelEditoria)
        
        imageViewBackground.leftAnchor.constraint(equalTo: self.contentView.leftAnchor).isActive = true
        imageViewBackground.rightAnchor.constraint(equalTo: self.contentView.rightAnchor).isActive = true
        imageViewBackground.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        imageViewBackground.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        
        labelTitulo.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 20).isActive = true
        labelTitulo.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -20).isActive = true
        labelTitulo.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -20).isActive = true
        
        labelEditoria.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        labelEditoria.bottomAnchor.constraint(equalTo: labelTitulo.topAnchor, constant: -10).isActive = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

extension PrincipalCell {
    
    /// Método responsável por exibir os dados do conteúdo da notícia na tela.
    /// - Parameter conteudo: conteúdo da notícia.
    func setValuesViews(conteudo: Conteudo) {
        labelTitulo.text = conteudo.titulo
        labelEditoria.text = conteudo.secao.nome.uppercased()
        
        imageViewBackground.kf.indicatorType = .activity
        if (!conteudo.imagens.isEmpty) {
            imageViewBackground.kf.setImage(with: URL(string: conteudo.imagens[0].url))
        }
    }
}
