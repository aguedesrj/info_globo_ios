//
//  HomeRouter.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import UIKit

final class HomeRouter {

    /// Método responsável por exibir a tela de detalhe da notícia.
    /// - Parameters:
    ///   - navigation: UINavigationController
    ///   - conteudo: conteúdo da notícia
    func show(at navigation: UINavigationController, conteudo: Conteudo!) {
        let controller: DetalheViewController = DetalheViewController()
        controller.conteudo = conteudo
        
        navigation.show(controller, sender: nil)
    }
}
