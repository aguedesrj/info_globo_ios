//
//  AppDelegate.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = UINavigationController(rootViewController: HomeViewController())
        
        if #available(iOS 11.0, *) {
            UINavigationBar.appearance().barTintColor = UIColor(named: "colorNavigation")
        } else {
            UINavigationBar.appearance().barTintColor = UIColor(red: 0.0/255.0,
                                                                green: 45.0/255.0,
                                                                blue: 136.0/255.0,
                                                                alpha: 1.0)
        }
        
        UINavigationBar.appearance().barStyle = .black
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        self.window = window
        window.makeKeyAndVisible()
        return true
    }

}

