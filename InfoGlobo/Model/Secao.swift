//
//  Secao.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import Foundation
import SwiftyJSON

struct Secao {
    var nome: String
    var url: String
    init(nome: String, url: String) {
        self.nome = nome
        self.url = url
    }
}

extension Secao {
    
    /// Método responsável por realizar o parser do JSON no objeto Secao
    /// - Parameter json: JSON retornado do serviço
    /// - Returns: retorno com a lista de Secao.
    static func parse(json: [String: JSON]) -> Secao {
        let nome = json["nome"]!.stringValue
        let url = json["url"]!.stringValue
        
        return self.init(nome: nome, url: url)
    }
}
