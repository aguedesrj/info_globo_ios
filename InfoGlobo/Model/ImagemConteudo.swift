//
//  ImagemConteudo.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import Foundation
import SwiftyJSON

struct ImagemConteudo {
    var autor: String
    var fonte: String
    var legenda: String
    var url: String
    init(autor: String, fonte: String, legenda: String, url: String) {
        self.autor = autor
        self.fonte = fonte
        self.legenda = legenda
        self.url = url
    }
}

extension ImagemConteudo {
    
    /// Método responsável por realizar o parser do JSON na lista de Imagem
    /// - Parameter json: JSON retornado do serviço
    /// - Returns: retorno com a lista de imagens.
    static func parse(json: [JSON]) -> [ImagemConteudo] {
        var listaReturno: [ImagemConteudo] = []
        for item in json {
            let autor   = item["autor"].stringValue
            let fonte   = item["fonte"].stringValue
            let legenda = item["legenda"].stringValue
            let url     = item["url"].stringValue
            
            listaReturno.append(self.init(autor: autor, fonte: fonte, legenda: legenda, url: url))
        }
        return listaReturno
    }
}
