//
//  Conteudo.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import Foundation
import SwiftyJSON

struct Conteudo {
    var autores: String
    var informePublicitario: Bool
    var subTitulo: String
    var texto: String
    var publicadoEm: String
    var secao: Secao
    var tipo: String
    var titulo: String
    var url: String
    var urlOriginal: String
    var imagens: [ImagemConteudo]
    
    init(autores: String, informePublicitario: Bool, subTitulo: String, texto: String,
         publicadoEm: String, secao: Secao,
         tipo: String, titulo: String, url: String, urlOriginal: String, imagens: [ImagemConteudo]) {
        
        self.autores = autores
        self.informePublicitario = informePublicitario
        self.subTitulo = subTitulo
        self.texto = texto
        self.publicadoEm = publicadoEm
        self.secao = secao
        self.tipo = tipo
        self.titulo = titulo
        self.url = url
        self.urlOriginal = urlOriginal
        self.imagens = imagens
    }
}

extension Conteudo {
    
    /// Método responsável por realizar o parser do JSON e transformar nume lista de Conteudo
    /// - Parameter json: JSON retornado do serviço
    /// - Returns: retorno com a lista de Conteudo.
    static func parse(json: [JSON]) -> [Conteudo] {
        var listaReturno: [Conteudo] = []
        for item in json[0][0]["conteudos"].arrayValue {
            
            var autores: String = ""
            for autor in item["autores"].arrayValue {
                if (autores.count > 0) {
                    autores.append(", ")
                }
                autores.append(autor.stringValue)
            }
            
            let informePublicitario = item["informePublicitario"].boolValue
            let subTitulo           = item["subTitulo"].stringValue
            let texto               = item["texto"].stringValue
            let publicadoEm         = converterData(data: item["publicadoEm"].stringValue)
            let secao               = Secao.parse(json: item["secao"].dictionaryValue)
            let tipo                = item["tipo"].stringValue
            let titulo              = item["titulo"].stringValue
            let url                 = item["url"].stringValue
            let urlOriginal         = item["urlOriginal"].stringValue
            let imagens             = ImagemConteudo.parse(json: item["imagens"].arrayValue)
            
            listaReturno.append(self.init(autores: autores, informePublicitario: informePublicitario, subTitulo: subTitulo,
                                          texto: texto, publicadoEm: publicadoEm, secao: secao,
                                          tipo: tipo, titulo: titulo, url: url, urlOriginal: urlOriginal,
                                          imagens: imagens))
        }
        return listaReturno
    }

    /// Método responsável por converte a data do JSON em data formatada pra ser exibida na tela.
    /// - Parameter data: data retornada do JSON
    /// - Returns: data com o formato em dd/MM/yy HH:mm
    static fileprivate func converterData(data: String) -> String {
       let dateFormatter = DateFormatter()
       dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
       let date = dateFormatter.date(from: data)
        
       dateFormatter.dateFormat = "dd/MM/yy HH:mm"
       return dateFormatter.string(from: date!)
   }
}
