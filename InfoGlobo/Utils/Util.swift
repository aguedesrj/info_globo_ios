//
//  Util.swift
//  InfoGlobo
//
//  Created by André Lessa Guedes.
//

import UIKit


/// Classe que contém métodos útéis para o app.
final class Util {
    
    /// Método responsável por customizar o tamanho da fonte(bold) para o iPhone4 e iPhone5
    /// - Parameter sizeFont: tamanho da fonte
    /// - Returns: fonte com o tamanho alterado.
    class func customSizeFonteBold(sizeFont: CGFloat) -> UIFont {
        if (UIScreen.main.nativeBounds.size.height == 960.0) {
            return UIFont.boldSystemFont(ofSize: (sizeFont - 3))
        } else if (UIScreen.main.nativeBounds.size.height == 1136.0) {
            return UIFont.boldSystemFont(ofSize: (sizeFont - 2))
        }
        return UIFont.boldSystemFont(ofSize: sizeFont)
    }
    
    /// Método responsável por customizar o tamanho da fonte para o iPhone4 e iPhone5
    /// - Parameter sizeFont: tamanho da fonte
    /// - Returns: fonte com o tamanho alterado.
    class func customSizeFonte(sizeFont: CGFloat) -> UIFont {
        if (UIScreen.main.nativeBounds.size.height == 960.0) {
            return UIFont.systemFont(ofSize: (sizeFont - 3))
        } else if (UIScreen.main.nativeBounds.size.height == 1136.0) {
            return UIFont.systemFont(ofSize: (sizeFont - 2))
        }
        return UIFont.systemFont(ofSize: sizeFont)
    }
}
